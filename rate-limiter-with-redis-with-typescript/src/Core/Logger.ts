import { createLogger, transports, format } from 'winston';
import * as fs from 'fs';
import * as path from 'path';
import * as DailyRotateFile from 'winston-daily-rotate-file';

let dir = process.env.LOG_DIR;
if (!dir) dir = path.resolve('logs');

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

const logLevel = process.env.LOG_LEVEL || 'debug';

const options = {
    file: {
        level: logLevel,
        filename: dir + '/%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        timestamp: true,
        handleExceptions: true,
        humanReadableUnhandledException: true,
        prettyPrint: true,
        json: true,
        maxSize: '20m',
        colorize: true,
        maxFiles: '14d',
    },
};

export default createLogger({
    transports: [
        new transports.Console({
            level: logLevel,
            format: format.combine(format.errors({ stack: true }), format.prettyPrint()),
        }),
        new transports.File({
            level: logLevel,
            filename: dir + '/applog.log'
        })
    ],
    format: format.combine(  
        format.timestamp({
            format: 'YYYY-mm-dd hh:mm:ss'
        }),
        format.printf(info => `[${[info.timestamp]}] local.${info.level.toUpperCase()}: ${info.message}`)
    ),
    exceptionHandlers: [new DailyRotateFile(options.file)],
    exitOnError: false, // do not exit on handled exceptions
});
