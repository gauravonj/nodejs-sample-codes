"user strict";
import * as redis from "redis";
import {REDIS_HOST, REDIS_PORT} from "../AppConfig";

class RedisDatabase {
  private options: any;
  constructor() {
    this.options = {
      host: REDIS_HOST,
      port: REDIS_PORT,
    };
  }

  /**
   * @function connectDB
   * Function will connect with redis database and return the client object.
   */
  connectDB() {
    const client = redis.createClient(this.options);
    client.on("error", (err: Error) => {
      console.error("Redis Error ::", err);
    });

    client.on("ready", (err: Error) => {
      if (err) console.log("Redis Ready Error :: ", err);
      console.log("Redis is ready");
    });

    return client;
  }
}

export default new RedisDatabase().connectDB();