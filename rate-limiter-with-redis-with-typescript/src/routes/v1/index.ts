import * as express from "express";
import UserRoute from './UserRoute';
import OtherRoute from './OtherRoute';
import BookRoute from './BookRoute';
const router = express.Router();

router.use('/', new OtherRoute().load());
router.use('/user', new UserRoute().load());
router.use('/book', new BookRoute().load());


export default router;