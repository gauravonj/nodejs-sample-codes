import * as express from "express";
import * as fs from 'fs';
import * as path from 'path';
class OtherRoute {
  public load(): express.Router {
    const router = express.Router();
    router.get("/", (req: express.Request, res: express.Response) => {
      res.send("api is working fine - Home route");
    });
    router.get("/play", (req: express.Request, res: express.Response) => {
      res.writeHead(200, { 'Content-Type': 'video/mp4' })

      const videoStream = fs.createReadStream(path.join(__dirname, '../../../assets/NASA.mp4'));
      videoStream.pipe(res);
    });

    return router;
  }
}

export default OtherRoute;
