import * as express from "express";
import MainRoute from "./MainRoute";

/**
 * UserRoute class description here.
 */
class UserRoute extends MainRoute {
   
    public  load(): express.Router {
        const router = express.Router();
        
        router.get("/", [this.apiRateLimiter.useRemoteAddress()], (req: express.Request, res: express.Response) => {
            res.send("api is working fine");
        });

        router.get("/user-detail", (req: express.Request, res: express.Response) => {
            res.send("user deatils listing api is working fine");
        });

        return router;
    }

}

export default UserRoute;
