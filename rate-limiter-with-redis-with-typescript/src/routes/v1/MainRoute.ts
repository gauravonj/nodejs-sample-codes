import * as express from "express";
import ApiLimiter from '../../Core/ApiLimiter';

class MainRoute {
    public apiRateLimiter: ApiLimiter;
    constructor() {
        this.apiRateLimiter = new ApiLimiter(express());
    }
}

export default MainRoute;
