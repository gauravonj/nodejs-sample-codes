import * as express from "express";
import { Types } from 'mongoose';
import MainRoute from "./MainRoute";
import { BookModel, Book } from '../../Database/model/Book';
import BookRepo from '../../Database/repository/BookRepo';
/**
 * BookRoute class description here.
 */
class BookRoute extends MainRoute {

    public load(): express.Router {
        const router = express.Router();

        router.get("/", /* [this.apiRateLimiter.useRemoteAddress()], */ (req: express.Request, res: express.Response) => {
            BookRepo.find().then((books: Book[] | null) => {
                res.json({
                    'message': 'books listing',
                    'data': books
                })
            }).catch((err) => {
                res.json({
                    "error": err
                })
            })
        });

        router.get("/:id", (req: express.Request, res: express.Response) => {
            const id = Types.ObjectId(req.params.id);
            BookRepo.findById(id).then((book) => {
                res.json({
                    'message': 'book data',
                    'data': book
                })
            }).catch((err) => {
                res.json({
                    "error": err
                })
            })
        });

        router.delete('/:id', (req: express.Request, res: express.Response) => {
            const id = Types.ObjectId(req.params.id);
            BookRepo.findById(id).then((bookData) => {
                BookRepo.delete(id).then((result) => {
                    res.json({
                        'message': 'book deleted successfully',
                        'data': bookData
                    });
                }).catch((err) => {
                    res.json({
                        "error": err
                    })
                })
            })
        });

        router.put("/:id", (req: express.Request, res: express.Response) => {

            const id = Types.ObjectId(req.params.id);
            BookRepo.findById(id).then((bookData) => {
                const book = new BookModel(req.body);
                BookRepo.update(id, book).then((bookData) => {
                    res.json({
                        'message': 'book updated',
                        'data': bookData
                    })
                }).catch((err) => {
                    res.json({
                        "data": [],
                        "error": err
                    })
                })
            }).catch((err) => {
                res.json({
                    "error": err
                })
            })
        });

        router.post("/", (req: express.Request, res: express.Response) => {
            const book = new BookModel(req.body);

            BookRepo.create(book).then((data) => {
                res.json({
                    'message': 'book added',
                    'data': data
                })
            }).catch((err) => {
                res.json({
                    "error": err
                })
            });
        });

        return router;
    }

}

export default BookRoute;
