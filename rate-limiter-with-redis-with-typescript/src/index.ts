import * as express from 'express';
import * as dotenv from 'dotenv';
dotenv.config();
import * as cors from 'cors';
import * as helmet from 'helmet';
import BaseController from './BaseController';
import './Database';
import router from './routes/v1';
import { FailureMsgResponse } from './Core/ApiResponse';


import { PORT, OPTION_SUCCESS_STATUS, APP_NAME, HOST, CORS_URL } from './AppConfig';

class Server extends BaseController {
    private _port: number;
    private _appname: string;
    private _host: string;
    private _corsUrl: string;
    private _optionsSuccessStatus: number;


    constructor() {
        super();
        this._port = parseInt(PORT as string, 10) || 4000;
        this._optionsSuccessStatus = parseInt(OPTION_SUCCESS_STATUS as string, 10) || 200;
        this._appname = APP_NAME as string || 'Nodejs App';
        this._host = HOST as string || `localhost`;
        this._corsUrl = CORS_URL as string || '*';
    }

    loadMiddleware() {
        this.app.use(helmet());
        this.app.use(cors({ origin: this._corsUrl, optionsSuccessStatus: this._optionsSuccessStatus }));
        this.app.use(express.json());
    }

    loadApiRoutes() {

        this.app.use('/v1', router);
        this.app.use((req, res, next) => {
            next(new FailureMsgResponse("api route not found").send(res))
        });
    }

    start() {
        this.loadMiddleware();
        this.loadApiRoutes();
        this.app.listen(this._port, () => {
            console.log(`${this._appname} started on ${this._host}:${this._port}`);
        });
    }
}

// Create an instance of Server class and starting the application.
const server = new Server();
server.start();
