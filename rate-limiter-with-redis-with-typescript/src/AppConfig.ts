export const ENVIRONMENT = process.env.NODE_ENV;
export const PORT = process.env.PORT;
export const HOST = process.env.HOST;
export const CORS_URL = process.env.CORS_URL;
export const OPTION_SUCCESS_STATUS = process.env.OPTION_SUCCESS_STATUS;
export const REDIS_PORT = process.env.REDIS_PORT || '6379';
export const REDIS_HOST = process.env.REDIS_HOST || 'localhost';
export const APP_NAME = process.env.APP_NAME || 'my app';
export const db = {
    name: process.env.DB_NAME || '',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '27107',
    user: process.env.DB_USER || '',
    passwords: process.env.DB_PASS || ''
}
