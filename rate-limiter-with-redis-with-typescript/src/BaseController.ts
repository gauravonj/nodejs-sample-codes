import * as express from 'express';
class BaseController {
    public app: express.Express;

    constructor() {
        this.app = express();
    }
}
export default BaseController;