import { model, Schema, Document } from 'mongoose';

export const DOCUMENT_NAME = 'Book';
export const COLLECTION_NAME = 'books';

export interface Book extends Document {
    title: string;
    author: string;
    year: number;
    pages: number;
    created_at: Date;
}

const BookSchema: Schema = new Schema(
    {
        title: { type: String, required: true },
        author: { type: String, required: true },
        year: { type: Number, required: true },
        pages: { type: Number, required: true },
        created_at: { type: Date, default: Date.now() },
    },
    {
        versionKey: false
    }
);

BookSchema.pre<Book>("save", next => {
    if (!this.created_at) {
        this.created_at = Date.now();
    }
    next();
});


export const BookModel = model<Book>(DOCUMENT_NAME, BookSchema);