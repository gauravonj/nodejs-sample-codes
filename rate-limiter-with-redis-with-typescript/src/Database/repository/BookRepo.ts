import { Book, BookModel } from "../model/Book";
import { Types } from "mongoose";
export default class BookRepo {
    public static findById(id: Types.ObjectId): Promise<Book | null> {
        return BookModel.findOne({ _id: id })
            .select("+title +author +pages +year")
            .lean<Book>()
            .exec();
    }

    public static find(): Promise<Book[] | null> {
        return BookModel.find().lean<Book[]>().exec();
    }

    public static async create(book: Book): Promise<Book> {
        return await BookModel.create(book);
    }

    public static update(id: Types.ObjectId, book: Book): Promise<any> {
        return BookModel.updateOne(
            {
                _id: id,
            },
            {
                $set: {
                    "title": book.title,
                    "author": book.author,
                    "year": book.year,
                    "pages": book.pages
                },
            }
        ).lean().exec();
    }

    public static delete(id: Types.ObjectId): Promise<any> {
        return BookModel.deleteOne(
            {
                _id: id,
            }
        ).lean().exec();
    }
}
