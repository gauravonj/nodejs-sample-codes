"use strict";
const ApiLimiter = require("./config");
class Routes {
  constructor(app) {
    this.app = app;
    this.apiRateLimiter = new ApiLimiter(app);
  }

  appRoutes() {
    this.app.get("/", (req, res) => {
      res.status(200).json({
        status: "ok",
        message: "rate limiter api demo is working",
        data: [],
      });
    });

    this.app.get(
      "/user",
      this.apiRateLimiter.useRemoteAddress(),
      (req, res) => {
        res.status(200).json({
          status: "ok",
          message: "users listed successfully",
          data: [
            {
              username: "testuser1001",
              name: "Test User",
            },
            {
              username: "testuser1002",
              name: "Test User",
            },
          ],
        });
      }
    );

    this.app.get(
      "/user/:id",
      this.apiRateLimiter.asGetParameter(),
      (req, res) => {
        res.status(200).json({
          status: "ok",
          message: "user details listed",
          data: {
            username: "testuser1001",
            name: "Test User",
          },
        });
      }
    );

    this.app.get(
      "/details/:apiKey",
      this.apiRateLimiter.checkApiKey(),
      (req, res) => {
        res.status(200).json({
          status: "ok",
          message: "checking with api key : " + req.params.apiKey,
        });
      }
    );
  }

  routeConfig() {
    this.appRoutes();
  }
}

module.exports = Routes;
