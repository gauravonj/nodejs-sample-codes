"user strict";
const redis = require("redis");
class RedisDatabase {
  constructor() {
    this.options = {
      host: "127.0.0.1",
      port: 6379,
    };
  }

  connectDB() {
    const client = redis.createClient(this.options);
    client.on("error", (err) => {
      console.error("Redis Error ::", err);
    });

    client.on("ready", (err) => {
      if (err) console.log("Redis Ready Error :: ", err);
      console.log("Redis is ready");
    });

    return client;
  }
}
module.exports = new RedisDatabase().connectDB();
