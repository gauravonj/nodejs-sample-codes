"use strict";
const express = require("express");
const http = require("http");
const bodyparser = require("body-parser");
const Routes = require("./routes");

class Server {
  constructor() {
    this.appName = process.env.APP_NAME || "API RATE LIMITER";
    this.port = process.env.PORT || 9000;
    this.host = process.env.HOST || "localhost";
    this.app = express();
    this.http = http.Server(this.app);
  }

  appConfig() {
    this.app.use(bodyparser.json());
  }

  appRoutes() {
    new Routes(this.app).routeConfig();
  }

  appExecute() {
    this.appConfig();
    this.appRoutes();
    this.http.listen(this.port, this.host, () => {
      console.log(
        `${this.appName} started on server - ${this.host}:${this.port}`
      );
    });
  }
}

const myApp = new Server();
myApp.appExecute();
