"user strict";

const client = require("./redisDB");
const expressLimiter = require("express-limiter");
class ApiLimiter {
  constructor(app) {
    this.limiter = expressLimiter(app, client);
  }

  useRemoteAddress() {
    return this.limiter({
      path: "/user",
      method: "get",
      lookup: ["connection.remoteAddress"],
      total: 5,
      expire: 1000 * 60 * 60,
      onRateLimited: function (request, response, next) {
        response
          .status(429)
          .json("You are not welcome here, Rate limit exceeded");
      },
    });
  }

  asGetParameter() {
    return this.limiter({
      path: "/user/:id",
      method: "get",
      lookup: ["params.id"],
      total: 5,
      expire: 1000 * 60 * 60,
      onRateLimited: function (request, response, next) {
        response
          .status(429)
          .json("api cannot be accessed for requested parameter");
      },
    });
  }

  checkApiKey() {
    return this.limiter({
      path: "/details/",
      method: "get",
      lookup: async (request, response, opts, next) => {
        try {
          const validKeyResult = await this.isValidApiKey(
            request.params.apiKey
          );
          if (validKeyResult) {
            opts.lookup = "params.apiKey";
            opts.total = 3;
          } else {
            opts.lookup = "connection.remoteAddress";
            opts.total = 5;
          }
        } catch (error) {
          opts.lookup = "connection.remoteAddress";
          opts.total = 5;
        }
        return next();
      },
      total: 5,
      expire: 1000 * 60 * 60,
      onRateLimited: function (request, response, next) {
        response
          .status(429)
          .json("You are not welcome here, Rate limit exceeded For API key access");
      },
    });
  }

  isValidApiKey(apiKey) {
    /**
     * Here based on `apiKey` you should rreturn true or false.
     *`apiKey` can be compared with any api key stored in database.
     */
    return new Promise((resolve, reject) => {
      1 ? resolve(true) : reject(false);
    });
  }
}

module.exports = ApiLimiter;
